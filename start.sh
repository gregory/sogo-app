#!/bin/bash

set -eu -o pipefail

echo "=> Generating sogo.conf"
sed -e "s,##MYSQL_URL##,${MYSQL_URL}," \
    -e "s,##LDAP_URL##,${LDAP_URL}," \
    -e "s/##LDAP_BIND_DN##/${LDAP_BIND_DN}/" \
    -e "s/##LDAP_BIND_PASSWORD##/${LDAP_BIND_PASSWORD}/" \
    -e "s/##LDAP_USERS_BASE_DN##/${LDAP_USERS_BASE_DN}/" \
    -e "s/##MAIL_IMAP_SERVER##/${MAIL_IMAP_SERVER}/" \
    -e "s/##MAIL_IMAP_PORT##/${MAIL_IMAP_PORT}/" \
    -e "s/##MAIL_SMTP_SERVER##/${MAIL_SMTP_SERVER}/" \
    -e "s/##MAIL_SMTP_PORT##/${MAIL_SMTP_PORT}/" \
    -e "s/##MAIL_SIEVE_SERVER##/${MAIL_SIEVE_SERVER}/" \
    -e "s/##MAIL_SIEVE_PORT##/${MAIL_SIEVE_PORT}/" \
    -e "s/##MAIL_DOMAIN##/${MAIL_DOMAIN}/" \
    /app/code/sogo.conf  > /run/sogo.conf

echo "=> Generating nginx.conf"
sed -e "s,##HOSTNAME##,${APP_DOMAIN}," \
    /app/code/nginx.conf  > /run/nginx.conf

echo "=> Ensure directories"
mkdir -p /run/GNUstep /run/nginx

echo "=> Make cloudron own the data"
mkdir -p /app/data/spool
chown -R cloudron:cloudron /run /app/data

echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i SOGo

FROM cloudron/base:0.10.0
MAINTAINER SOGo developers <support@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code


RUN apt-get update && apt-get install -y apt-transport-https && rm -rf /var/cache/apt /var/lib/apt/lists /etc/ssh_host_*

# We can only use nightly builds without a license
RUN echo "deb https://packages.inverse.ca/SOGo/nightly/3/ubuntu xenial xenial" >> "/etc/apt/sources.list"
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 19CDA6A9810273C4

RUN apt-get update && apt-cache search sogo && apt-get install -y sogo memcached && rm -rf /var/cache/apt /var/lib/apt/lists /etc/ssh_host_*

RUN rm /etc/sogo/sogo.conf && ln -s /run/sogo.conf /etc/sogo/sogo.conf && \
rm -rf /var/log/nginx && ln -s /run/nginx /var/log/nginx && \
mkdir /run/GNUstep && ln -s /run/GNUstep /home/cloudron/GNUstep

# configure supervisor
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
ADD supervisor/ /etc/supervisor/conf.d/

ADD sogo.conf nginx.conf start.sh /app/code/

RUN chown -R cloudron:cloudron /etc/sogo
# Create the spool directory on /run and symlink the original one
RUN rmdir /var/spool/sogo && mkdir -p /app/data/spool && ln -s /app/data/spool /var/spool/sogo

CMD [ "/app/code/start.sh" ]
